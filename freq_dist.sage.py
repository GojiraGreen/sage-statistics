#!/usr/bin/env python
"""Statistics helper functions

Usage:
    Import to sage terminal or scripts.
"""
import numpy as np


def freq_dist(data, bins):
    """Histogram of a freqency distribution

    Args:
        list: Numeric data.
        list: Numerical range for frequency

    Returns:
        Product of numpy histogram() function.
    """
    values = sorted(list(set(data)))
    freqs = []

    for value in values:
        freqs.append(data.count(value))

    return np.histogram(values, bins, weights=freqs)
